/* RFQ Coupler Conditioning SNL Program */


/* Author: Emmanouil Trachanas*/
/* Email:  Emmanouil.Trachanas@ess.eu */

/*Previous Versions:Muyuan Wang (TS2-ESS), A. Gaget (CEA)   */


program sncConditioning


/* C language libraries */
%%#include <string.h>
%%#include <time.h>
%%#include <stdlib.h>
%%#include <stdio.h>

/*question 1*/
/*=================== Options ======================*/
option +r;
option +s;
option -c;
/*=================================================*/

/*=================== Declarations ======================*/




/* start/stop Button */
int startButton;
assign startButton to "{M}:Button";
monitor startButton;


/*Infinite Procedure Button */
int neverStop;
assign neverStop to "{M}:Inf";
monitor neverStop;

/* Status message */
string message;
assign message to "{M}:Msg";
monitor message;

/* power control parameters */

/* Minimum Power */

float Pmin;
assign Pmin to "{M}:Power-min";
monitor Pmin;

/* Maximum Power */
float Pmax;
assign Pmax to "{M}:Power-max";
monitor Pmax;

/*Power Floor */
float PowFloor;
assign  PowFloor to "{M}:Power-Floor";
monitor  PowFloor;


/*Power Step Up */
float StepUp;
assign StepUp to "{M}:Power-Step-Up";
monitor StepUp;

/*Power Step Down */
float StepDown;
assign StepDown to "{M}:Power-Step-Down";
monitor StepDown;

/*Power to klystron */
float Pklystron;
assign Pklystron to "RFQ-010:RFS-DIG-101:RFCtrlCnstSP-Mag";
monitor Pklystron;

/*---------------------------------*/

/*RF pulse Control Parameters*/

/* Plateau Time */
float pulsePlateau;
assign pulsePlateau to "{M}:Plateau-Time";
monitor pulsePlateau;


/* RF Pulse width Step */
float StepWidth;
assign StepWidth to "{M}:Pulse-Width-Step";
monitor StepWidth;

/* Minimum RF Pulse width */
float widthmin;
assign widthmin to "{M}:Pulse-Width-Min";
monitor widthmin;

/* Maximum RF Pulse width */
float widthmax;
assign widthmax to "{M}:Pulse-Width-Max";
monitor widthmax;

/* Count Step Pulse */
float nbPulsestep;
assign nbPulsestep to "{M}:Count-Pulse-Step";
monitor nbPulsestep;


/*Interlock Detection*/

/* Interlock ---- Critical Fault inputs from FIM  (arc detectors, electron pick ups etc) */
int Criticalfault;
assign Criticalfault to "{M}:Critical-Fault";
monitor Criticalfault;

/* Stores the current power in case of a fault   */   
float ItckPower;
assign ItckPower to "{M}:Itck-Power";

/* Vacuum Fault -- Assigned to soft threshold in software */
int Vacfault;
assign Vacfault to "{M}:Vacuum-Fault";
monitor Vacfault;

/* Power Fault ---- Comparison between Set and readback value*/
int Powerfault;
assign Powerfault to "{M}:Power-Fault";
monitor Powerfault;

/*Settings to EVM RF system*/

/* Repetition Rate Set Point */
float RepFreq;
assign RepFreq to "TR-RFQ-LLRF1:CycleFreq-SP";

/* RF Pulse width Set Point */
float width;
assign width to "TR-RFQ-LLRF1:RFSyncWdt-SP";

/* Count Pulse */
int countPulse;
assign countPulse to "TR-RFQ-LLRF1:Cycle-Cnt";
monitor countPulse;
/*=================================================*/


/*=========== RF Distribution Power System (Ramp) ===========*/
/*Definition of waveform for Ramp Up function of Klystron */
int Rampnumber;
assign Rampnumber to "RFQ-LLRF1::SPTblRampSmpNm";

/* Start of Magnitude Array */
float RampStrMag;
assign RampStrMag to "RFQ-LLRF1::SPTblRampStr-Mag";

/* End of Magnitude Array */
float RampEndMag;
assign RampEndMag to "RFQ-LLRF1::SPTblRampEnd-Mag";

/* Start of Angle Array */
float RampStrAng;
assign RampStrAng to "RFQ-LLRF1::SPTblRampStr-Ang";

/* End of Angle Array */
float RampEndAng;
assign RampEndAng to "RFQ-LLRF1::SPTblRampEnd-Ang";

/* Commit Table to LLRF Firmware */
int CommitToFW;
assign CommitToFW to "RFQ-010:RFS-DIG-101:SPTbl-TblToFW";



/*=================================================*/

/*==================== Variable Settings ====================*/
int countPulsestep=0;
int start	=1; // start operation;
int floor	=0; // power floor;
int soak	=0; // plateau state;
int cnt		=0; // cycle counter(Debug Mode);
int os		=0; // operation stop;
int cf		=0; // llrf critical fault;
int firstcycle	=0; // first run(minimum power);
int flag	=1; // vacuum fault flag;

/*====================== State Sets ======================*/

ss ss2{

  state init
  {      


	 when (os==1 && startButton == 0) 
	 {
		 sprintf(message,"Operator Stop");
		 pvPut(message);
	         printf("Operator Stop\n");	
	 } state init

	 
	 when (cf==1 && Criticalfault==1) 
	 {
		 sprintf(message,"Critical Fault");
		 pvPut(message);		 
	 } state init

         when (startButton==1 && Criticalfault==1)
 	 {      
	    sprintf(message,"Critical Fault");
	    pvPut(message);  
	 } state init


 	 when (os==0 && startButton == 0)
	 {       
	    sprintf(message,"Press Start");
	    pvPut(message);	
         } state init


	 when (startButton == 1 && Criticalfault==0 && Powerfault==1)
	 {      	
	     sprintf(message,"Power Fault-Difference Between Sepoint and Readback value\n");
	     pvPut(message);	 
	 } state init
 
	
	 when (startButton == 1 && Criticalfault==0 && Powerfault==0)
	 {        
		  os=0;
		  cf=0;
  		  Rampnumber=5;
            	  pvPut(Rampnumber,SYNC);
            	  RampStrMag=0;
           	  pvPut(RampStrMag,SYNC);
           	  RampStrAng=0;
           	  pvPut(RampStrAng,SYNC);
            	  RampEndAng=0;
            	  pvPut(RampEndAng,SYNC);
		  if (Vacfault==1) 
		  {
		        sprintf(message,"Vacuum Fault, Waiting for vacuum improvement\n");
	                pvPut(message);
		        printf("Vacuum Fault, Waiting for vacuum improvement\n");

		  }
	          else 
		  {
		       sprintf(message,"Starting the sequence..\n");
	               pvPut(message);
		      
		  }
		  
	  } state CycleStart

   }



  state CycleStart
  {

		when (cnt==2)
		{
		        sprintf(message ,"End of Sequence\n");
			pvPut(message);
            		RampEndMag=0;
	    		pvPut(RampEndMag,SYNC);
	   		CommitToFW=1;
            		pvPut(CommitToFW,SYNC);	
            		startButton=0;
	    		pvPut(startButton,SYNC);
            		RepFreq=1;
            		pvPut(RepFreq,SYNC);
            		width=50;
            		pvPut(width,SYNC);
	    		floor=0;
	   		soak=0;
	    		if (flag == 0) {flag=1;}
	    		start=1; 
	         	cnt=0;
            		
		} state init

		when (startButton==0)
	        {
	    		os=1;
 			width=widthmin;
			RampEndMag=Pmin;
	   	        pvPut(RampEndMag,SYNC);
 	    		CommitToFW=1;
           		pvPut(CommitToFW,SYNC);
		} state init


                when (Criticalfault==1)
		{
			cf=1;
			pvGet(RampEndMag);
	    		ItckPower=RampEndMag;
	   		pvPut(ItckPower,SYNC);
           		RampEndMag=Pmin;
	   		pvPut(RampEndMag,SYNC);
 	   		CommitToFW=1;
           		pvPut(CommitToFW,SYNC);
		} state init


		when (Criticalfault == 0 && Vacfault==0)
		{

			pvGet(Pmin);
 			RampEndMag=Pmin;
                	pvPut(RampEndMag,SYNC);
                	CommitToFW=1;
                	pvPut(CommitToFW,SYNC);
			pvGet(Pmax);
               		pvGet(RepFreq);
	        	firstcycle=1;

			if (start==1)
                        {   
	                   countPulsestep=0;
                    	   width=widthmin;
		    	   pvPut(width,SYNC);
	            	   sprintf(message,"Commencing the Sequence");
		    	   pvPut(message);		
		    	   start=0;
			}
                        else
	                {
		    	   pvPut(width,SYNC);
		    	   floor=0;
		    	   soak=0;
	            	   if (flag == 0) {flag=1;}
	        	}

			sprintf(message,"Starting the Sequence: %.3fms",width/1000);
	    		pvPut(message);
	    		printf("Starting the Sequence : %.3fms\n",width/1000);
                        
		} state PowerControl

  }


 state nominal
 {

		when (startButton == 0)
   	        {
	    	    RampEndMag=Pmin;
	   	    pvPut(RampEndMag,SYNC);
 	            CommitToFW=1;
                    pvPut(CommitToFW,SYNC);
		    os=1;
		    width=widthmin;
                } state init

                when (Criticalfault == 1)
                {
		    cf=1;
		    pvGet(RampEndMag);
	   	    ItckPower=RampEndMag;
	    	    pvPut(ItckPower,SYNC);
            	    RampEndMag=Pmin;
	    	    pvPut(RampEndMag,SYNC);
 	    	    CommitToFW=1;
            	    pvPut(CommitToFW,SYNC);    
                } state init

		//Plateau
        	when (soak==1 && Criticalfault==0 && Vacfault==0) 
 		{
		} state PowerControl	
	
		//Vacuum Fault
		when (Vacfault==1 && flag==1)
		{
		} state PowerControl
	
		//Vacuum Fault recovery
		when (Vacfault==0 && flag==0)
		{
	    		flag=1;
		} state PowerControl

		when (countPulse-countPulsestep>=nbPulsestep)
		{      
	   		 if (firstcycle==1) {firstcycle=0;}
	   		 else
	   		 {
	        	 if (floor == 0) { if (RampEndMag >= PowFloor) {floor=1;} } 
	    		 }
	    		 printf("%i, %i, %i\n",countPulse,countPulsestep,(int)(nbPulsestep+0.5));
        	} state PowerControl

  }


 state PowerControl
 {

                 when (startButton == 0)
	         {
	    		 RampEndMag=Pmin;
	    		 pvPut(RampEndMag,SYNC);
 	    		 CommitToFW=1;
            		 pvPut(CommitToFW,SYNC); 
	 		 os=1;
			 width=widthmin;
		  }state init

 		when (Criticalfault == 1)
		  {
			cf=1;
			pvGet(RampEndMag);
	    		ItckPower=RampEndMag;
	    		pvPut(ItckPower,SYNC);
            		RampEndMag=Pmin;
	    		pvPut(RampEndMag,SYNC);
 	    		CommitToFW=1;
            		pvPut(CommitToFW,SYNC);
		  } state init



		  when (RampEndMag>=Pmax && Criticalfault==0 && Vacfault==0)
		  {       		 	 
	    		if (soak==1)
	    		{
               		   sprintf(message,"Reached Plateau");
			   pvPut(message);	        
			   printf("Reached Plateau\n");
			   countPulsestep=countPulse;
	    		}
		  } state plateau
		
   
		 when (Criticalfault==0 && Vacfault==0)
		 {
	    		countPulsestep=countPulse;
	    		if (firstcycle == 1) {}
	    		else
	    		{
                	   RampEndMag=RampEndMag+StepUp;
	    		   if (RampEndMag >= Pmax)
	    		   {
		    		RampEndMag=Pmax;
		    		soak=1;
	    		   }
	    		   pvPut(RampEndMag,SYNC);
			   CommitToFW=1;
                	   pvPut(CommitToFW,SYNC);
                           sprintf(message,"%.3fms,%.1fHz,%.1fkw",width/1000,RepFreq,RampEndMag);
	       	           pvPut(message);
	        	   printf("%.3fms,%.1fHz,%.1fkw\n",width/1000,RepFreq,RampEndMag);
	    	        }
        	} state nominal
	

	  	
		when (Criticalfault==0 && Vacfault==1)
		 {
	    		if (flag == 1) {flag=0;}
	    		if (soak == 1) {soak=0;}
	    		countPulsestep=countPulse;
            		RampEndMag=RampEndMag-StepDown;
	    		if (floor==0 && Pmin>=RampEndMag) {RampEndMag=Pmin;}
            		if (floor==1 && PowFloor>=RampEndMag) {RampEndMag=PowFloor;}
	    	        pvPut(RampEndMag,SYNC);
	   		CommitToFW=1;
            		pvPut(CommitToFW,SYNC);
	    		sprintf(message,"Fault happened! Power Decreasing");
	    		pvPut(message);
	   		
		} state nominal  
		  
    }



    state plateau
    {

		  when (startButton == 0)
  	          {
			
            		RampEndMag=Pmin;
	    		pvPut(RampEndMag,SYNC);
 	    		CommitToFW=1;
            		pvPut(CommitToFW,SYNC);	
			width=widthmin;
		        os=1;
    	          } state init

                 when (Criticalfault==1 || Vacfault==1)
		{
	    		printf(message,"Fault During Plateau\n");
 			pvPut(message);
		} state nominal

		when (delay(pulsePlateau))
		{
           		sprintf(message,"Cycle Finished!!");
   	    		pvPut(message);
	    		printf("%.3fms,%.1fHz Cycle Finished!!\n\n\n",width/1000,RepFreq);
	   		width=width+StepWidth;
                	if (width>=widthmax) 
	        	{
		    	     width=widthmax;
		    	     cnt++;
	            	     if (cnt==2 && neverStop==1)
		    	     {
		        	cnt=0;
		        	width=widthmin;
	            	     }
	        	}
	    
     	        }state CycleStart
	}
}


